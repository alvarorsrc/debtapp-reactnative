import React from 'react'
import {createStackNavigator, createBottomTabNavigator, createAppContainer} from 'react-navigation'

import LoadingScreen from './src/screens/Loading'
import LoginScreen from './src/screens/Login'
import RegisterScreen from './src/screens/Register'

import DashboardScreen from './src/screens/Dashboard'
import ProductScreen from './src/screens/Product'
import HistoryScreen from './src/screens/History'
import ConfigScreen from './src/screens/Config'
import MemberScreen from './src/screens/Member'
import DetailScreen from './src/screens/Detail'

import { Color } from './src/constants/Color';
import SvgCustom from './src/components/SvgCustom'

const AccountNav = createStackNavigator(
    {
        Dashboard : {
            screen : DashboardScreen
        },
        Members : {
            screen : MemberScreen,
        },
        Details : {
            screen : DetailScreen,
        }
    },
    {
        initialRouteName: 'Dashboard',
    }
)

const EntryNav = createStackNavigator(
    {
        Loading : {
            screen : LoadingScreen,
        },
        Main : {
            screen : createBottomTabNavigator(
                {
                    Cuentas : {
                        screen : AccountNav,
                        navigationOptions: {
                            tabBarIcon: ({ focused }) => {
                                let color = focused ? Color.WHITE : Color.GRAY_LIGHT_TAB
                                return (
                                    <SvgCustom
                                        name   = "account"
                                        width  = {20}
                                        height = {20}
                                        color  = {color}
                                    />
                                )
                            },
                        }
                    },
                    Productos : {
                        screen : createStackNavigator(
                            {
                                Product : {
                                    screen : ProductScreen
                                },
                            }
                        ),
                        navigationOptions: {
                            tabBarIcon: ({ focused }) => {
                                let color = focused ? Color.WHITE : Color.GRAY_LIGHT_TAB
                                return (
                                    <SvgCustom
                                        name   = "product"
                                        width  = {20}
                                        height = {20}
                                        color  = {color}
                                    />
                                )
                            },
                        }
                    },
                    Reportes : {
                        screen : createStackNavigator(
                            {
                                History : {
                                    screen : HistoryScreen
                                },
                            }
                        ),
                        navigationOptions: {
                            tabBarIcon: ({ focused }) => {
                                let color = focused ? Color.WHITE : Color.GRAY_LIGHT_TAB
                                return (
                                    <SvgCustom
                                        name   = "history"
                                        width  = {20}
                                        height = {20}
                                        color  = {color}
                                    />
                                )
                            },
                        }
                    },
                    Config : {
                        screen : ConfigScreen,
                        navigationOptions: {
                            tabBarIcon: ({ focused }) => {
                                let color = focused ? Color.WHITE : Color.GRAY_LIGHT_TAB
                                return (
                                    <SvgCustom
                                        name   = "configurate"
                                        width  = {20}
                                        height = {20}
                                        color  = {color}
                                    />
                                )
                            },
                        }
                    },
                },
                {
                    initialRouteName: 'Cuentas',
                    tabBarOptions: {
                        activeBackgroundColor: Color.PRIMARY,
                        inactiveBackgroundColor: Color.PRIMARY,
                        activeTintColor: Color.WHITE,
                        inactiveTintColor: Color.GRAY_LIGHT_TAB,
                        showLabel: false,
                    }
                }
            ),
        },
        Sync : {
            screen : createBottomTabNavigator(
                {
                    Login : {
                        screen : LoginScreen,
                        navigationOptions: {
                            tabBarIcon: ({ focused }) => {
                                let color = focused ? Color.WHITE : Color.GRAY_LIGHT_TAB
                                return (
                                    <SvgCustom
                                        name   = "login"
                                        width  = {20}
                                        height = {20}
                                        color  = {color}
                                    />
                                )
                            },
                        }
                    },
                    Register : {
                        screen : RegisterScreen,
                        navigationOptions: {
                            tabBarIcon: ({ focused }) => {
                                let color = focused ? Color.WHITE : Color.GRAY_LIGHT_TAB
                                return (
                                    <SvgCustom
                                        name   = "register"
                                        width  = {20}
                                        height = {20}
                                        color  = {color}
                                    />
                                )
                            },
                        }
                    }   
                },
                {
                    initialRouteName: 'Login',
                    tabBarOptions: {
                        activeBackgroundColor: Color.PRIMARY,
                        inactiveBackgroundColor: Color.PRIMARY,
                        activeTintColor: Color.WHITE,
                        inactiveTintColor: Color.GRAY_LIGHT_TAB,
                    }
                }
            ),
        }
    },
    {
        initialRouteName: 'Loading',
        defaultNavigationOptions: {
            header: null,
        }
    }
)

const App = createAppContainer(EntryNav)

export default App