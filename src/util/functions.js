const normalizeString = (str) => {
    str = str.toLowerCase()
    str = str.replace(/á/gi, "a")
    str = str.replace(/é/gi, "e")
    str = str.replace(/í/gi, "i")
    str = str.replace(/ó/gi, "o")
    str = str.replace(/ú/gi, "u")
    return str
}

export { normalizeString }