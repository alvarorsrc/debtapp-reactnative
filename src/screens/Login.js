import React, { Component } from 'react';
import {View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';

import { Color } from './../constants/Color'

export default class LoginScreen extends Component {
    static navigationOptions = {
        title: 'Iniciar Sesión',
        headerStyle: {
            backgroundColor: Color.PRIMARY,
        },
        headerTintColor: Color.WHITE,
    }

    render() {
        return (
            <View style = {styles.container}>
                <View>
                    <Text style = {styles.label}>
                        Email
                    </Text>
                    <TextInput style = {styles.input}
                        placeholder = "Escriba su correo electrónico"/>
                    <Text style = {styles.label}>
                        Contraseña
                    </Text>
                    <TextInput style = {styles.input}
                        placeholder = "Escriba su contraseña"/>
                    <View style = {styles.bottom}>
                        <TouchableOpacity style = {styles.button}
                            onPress={()=>{
                                this.props.navigation.navigate('Main')
                            }}>
                            <Text style = {{color: Color.WHITE,fontSize: 16,}}>
                                Ingresar
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }    
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    label: {
        fontSize: 18,
        color: Color.TEXT_LABEL,
        fontWeight: 'bold',
    },
    input: {
        fontSize: 16,
        borderBottomWidth: 1,
        borderBottomColor: Color.LINE,
        marginBottom: 15,
    },
    button: {
        padding: 15,
        paddingLeft: 25,
        paddingRight: 25,
        borderRadius: 3,
        backgroundColor: Color.SECOND,
    },
    bottom:{
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 15,
    },
})