import React, { Component } from 'react'
import { ScrollView, View, StyleSheet } from 'react-native'
import Modal from 'react-native-modal'

import { Color } from './../constants/Color'
import FabButton from './../components/FabButton'
import ItemList from './../components/ItemList'
import ConfirmDialog from './../components/ConfirmDialog'

export default class MemberScreen extends Component {
    static navigationOptions = {
        title: 'Miembros',
        headerStyle: {
            backgroundColor: Color.PRIMARY,
        },
        headerTintColor: Color.WHITE,
    }

    constructor(props){
        super(props)
        this.state = {
            members : [
                {
                    name : 'Pedro Guerra',
                    startDate: '10/08/2018',
                    detail : [
                        { product : 'lomo', quantity : 2},
                        { product : 'chaufa', quantity : 4},
                        { product : 'saltado', quantity : 1},
                    ]
                },
                {
                    name : 'Juan Hidalgo',
                    startDate: '10/08/2018',
                    detail : [
                        { product : 'inka personal', quantity : 2},
                        { product : 'chaufa', quantity : 2},
                    ]
                },
            ],

            currentMemberName:'',
            modalDeleteVisible: false,
        }
    }

    render(){

        let items = this.state.members.map((item)=>{
            return (
                <ItemList key = {item.name}
                    mainText = {item.name}
                    sideText = {item.startDate}
                    onlyDelete = {true}
                    onPress  = {() => {
                        this.props.navigation.navigate('Details', {member : item})
                    }}
                    onDelete = {() => this.setState({modalDeleteVisible : true, currentMemberName : item.name})}/>  
            )
        })

        const modalDelete = <Modal animationIn = "slideInUp"
                            isVisible       = {this.state.modalDeleteVisible}
                            onBackdropPress = {() => {
                                this.setState({ modalDeleteVisible: false })
                            }}>
                                <ConfirmDialog title = "¿Seguro que desea eliminar este miembro?"
                                    confirmText = "Eliminar"
                                    color       = {Color.RED_PRIMARY}
                                    onConfirm   = {()=>this.deleteMember()}
                                    onCancel = {()=>this.setState({ modalDeleteVisible: false })}/>
                            </Modal>

        return(
            <View style = {{ flex: 1 }}>
                <ScrollView contentContainerStyle = {styles.containerList}>
                    {items}
                </ScrollView>
                
                {modalDelete}

                <FabButton 
                    onTap = {()=>{
                        this.props.navigation.navigate('Details')
                    }}
                />
            </View>
        )
    }

    deleteMember() {
        this.removeMember(this.state.currentMemberName)

        this.setState({ modalDeleteVisible : false})
    }

    removeMember(name){
        let index = this.state.members.findIndex((member) => member.name === name)
        if(index !== undefined && index >= 0){
            this.setState(state => {
                state.members.splice(index, 1)
                return {
                    members : state.members
                }
            })
        }
    }

}

const styles = StyleSheet.create({
    containerList: {
        padding: 20,
        paddingTop: 0,
    },    
})