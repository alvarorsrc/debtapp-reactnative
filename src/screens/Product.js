import React, { Component } from 'react'
import { Text, View } from 'react-native'

import { Color } from './../constants/Color'

export default class ProductScreen extends Component {
    static navigationOptions = {
        title: 'Lista de Productos',
        headerStyle: {
            backgroundColor: Color.PRIMARY,
        },
        headerTintColor: Color.WHITE,
    }

    render(){
        return(
            <View>
                <Text>Texto de prueba</Text>
            </View>
        )
    }

}