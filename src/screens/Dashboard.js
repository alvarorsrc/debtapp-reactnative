import React, { Component } from 'react'
import { Text, TouchableOpacity, TextInput, View, ScrollView, StyleSheet } from 'react-native'
import Modal from 'react-native-modal'

import { Color } from './../constants/Color'

import SvgCustom from './../components/SvgCustom'
import FabButton from './../components/FabButton'
import InputDialog from './../components/InputDialog'
import ConfirmDialog from './../components/ConfirmDialog'
import ItemList from './../components/ItemList'

import {normalizeString} from './../util/functions'

export default class DashboardScreen extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            // title: 'Lista de Cuentas',
            headerStyle: {
                backgroundColor: Color.PRIMARY,
            },
            // headerTintColor: Color.WHITE,
            headerTitle: (
                <View style = {{ flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
                    <Text style = {{ paddingStart: 20, fontSize:18,color: Color.WHITE}}>Lista de Cuentas</Text>
                    <TouchableOpacity style = {{paddingEnd: 20}} onPress = {navigation.getParam('tapSearch')}>
                        <SvgCustom
                            name   = "search"
                            width  = {25}
                            height = {25}
                            color  = {Color.WHITE}
                        />
                    </TouchableOpacity>
                </View>
            ),
        }
    }

    constructor(props){
        super(props)
        this.state = {
            accounts : [
                // {
                //     name: 'José',
                //     startDate: '10/08/2018',
                //     endDate: '',
                //     userId: '',
                //     members: 11,
                // },
                // {
                //     name: 'José Alfonso',
                //     startDate: '10/08/2018',
                //     endDate: '',
                //     userId: '',
                //     members: 11,
                // },
                // {
                //     name: 'José Luis',
                //     startDate: '10/08/2018',
                //     endDate: '',
                //     userId: '',
                //     members: 11,
                // },
            ],
            modalInitVisible : true,
            modalAddVisible : false,
            modalEditVisible : false,
            modalDeleteVisible : false,
            modalFinishVisible: false,
            
            currentAccountName: '',

            filterVisible: false,
            filter: ''

        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ tapSearch: this.onTapSearch.bind(this) });
    }

    render(){
        const modalAdd = <Modal animationIn = "slideInUp"
                            isVisible       = {this.state.modalAddVisible}
                            onBackdropPress = {() => {
                                this.setState({ modalAddVisible: false })
                            }}>
                            <InputDialog title = "Nueva Cuenta"
                                phFirst     = "Nombre del responsable"
                                confirmText = "Agregar"
                                color       = {Color.BLUE_PRIMARY}
                                onConfirm   = {(accountName) => this.addAccount(accountName)}
                                onCancel = {() => this.setState({ modalAddVisible: false })}/>
                        </Modal>

        const modalEdit = <Modal animationIn    = "slideInUp"
                                isVisible       = {this.state.modalEditVisible}
                                onBackdropPress = {() => {
                                    this.setState({ modalEditVisible: false })
                                }}>
                                <InputDialog title = "Editar Cuenta"
                                    defaultFirst = {this.state.currentAccountName}
                                    phFirst     = "Nombre del responsable"
                                    confirmText = "Modificar"
                                    color       = {Color.GREEN_PRIMARY}
                                    onConfirm   = {(accountName) => this.editAccount(accountName)}
                                    onCancel = {()=>this.setState({ modalEditVisible: false })}/>
                            </Modal>

        const modalDelete = <Modal animationIn = "slideInUp"
                                isVisible       = {this.state.modalDeleteVisible}
                                onBackdropPress = {() => {
                                    this.setState({ modalDeleteVisible: false })
                                }}>
                                    <ConfirmDialog title = "¿Seguro que desea eliminar esta cuenta?"
                                        confirmText = "Eliminar"
                                        color       = {Color.RED_PRIMARY}
                                        onConfirm   = {()=>this.deleteAccount()}
                                        onCancel = {()=>this.setState({ modalDeleteVisible: false })}/>
                                </Modal>

        const modalFinish = <Modal animationIn = "slideInUp"
                                isVisible       = {this.state.modalFinishVisible}
                                onBackdropPress = {() => {
                                    this.setState({ modalFinishVisible: false })
                                }}>
                                    <ConfirmDialog title = "¿Finalizar esta cuenta?"
                                        confirmText = "Finalizar"
                                        color       = {Color.BLUE_PRIMARY}
                                        onConfirm   = {()=> this.finishAccount()}
                                        onCancel = {()=>this.setState({ modalFinishVisible: false })}/>
                                </Modal>

        let item = this.state.accounts.map((item)=>{
            if(this.state.filter.length > 0 && !normalizeString(item.name).includes(normalizeString(this.state.filter))){
                return null
            }
            return (
                <ItemList key = {item.name}
                    mainText = {item.name}
                    subText  = {item.members + ' personas'}
                    sideText = {item.startDate}
                    // onlyDelete = {true}
                    onPress  = {() => {
                        this.props.navigation.navigate('Members')
                    }}
                    onEdit = {() => this.setState({modalEditVisible : true, currentAccountName : item.name})}
                    onFinish = {() => this.setState({modalFinishVisible : true, currentAccountName : item.name})}
                    onDelete = {() => this.setState({modalDeleteVisible : true, currentAccountName : item.name})}/>  
            )
        })
        

        if(this.state.accounts.length){
            return(
                <View
                    style = {{ flex: 1 }}
                >
                    {this.state.filterVisible &&
                        <TextInput
                            value = {this.state.filter}
                            style = {styles.inputFilter}
                            placeholder = "Ingrese el nombre de la cuenta"
                            placeholderTextColor = {Color.GRAY_LIGHT}
                            autoFocus = {true}
                            onChangeText = {(name) => this.setState({filter : name})}
                        />
                    }
                    <ScrollView
                        contentContainerStyle = {styles.containerList}
                    >
                        {item}
                    </ScrollView>
                    <FabButton 
                        onTap = {()=>{
                            this.setState({modalAddVisible: true})
                        }}
                    />
                    {modalAdd}
                    {modalEdit}
                    {modalDelete}
                    {modalFinish}
                </View>
                
            )
        }else{
            return(
                <View style = {styles.container}>
                    <Text style = {styles.initText}>
                        Usted no tiene ninguna cuenta creada. Empiece una nueva cuenta ahora.
                    </Text>
                    <View style  = {styles.arrow}>
                        <SvgCustom
                            name   = "flexArrow"
                            width  = {130}
                            height = {130}
                            color  = {Color.GRAY_LIGHT_3}
                        />
                    </View>
                    <FabButton 
                        onTap = {()=>{
                            this.setState({modalAddVisible: true})
                        }}
                    />
                    <Modal
                        animationIn     = "slideInUp"
                        isVisible       = {this.state.modalInitVisible}
                        onBackdropPress = {() => {
                            this.setState({ modalInitVisible: false })
                        }}>
                        <View style={ styles.modalContainer}>
                            <Text style={styles.modalTitle}>
                                Antes de empezar
                            </Text>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({modalInitVisible: false})
                                    this.props.navigation.navigate('Sync')
                                }}
                                style={[styles.modalButton, { backgroundColor: Color.GREEN_PRIMARY }]}>
                                <Text style={[styles.modalButtonText, { color: Color.WHITE }]}>
                                    Iniciar Sesión Ahora
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.modalButton}
                                onPress={() => {
                                    this.setState({modalInitVisible: false})
                                }}>
                                <Text style={[styles.modalButtonText, { color: Color.GREEN_PRIMARY }]}>
                                    Continuar sin vincular
                                </Text>
                            </TouchableOpacity>

                        </View>
                    </Modal>
                    {modalAdd}
                </View>
            )
        }
    }


    onTapSearch() {
        this.setState(state => {
            return { 
                filterVisible : !state.filterVisible
            }
        })
    }
    

    addAccount(accountName){
        if(this.validateAccountName(accountName) === undefined){
            this.setState( state => { 
                return {
                    accounts : state.accounts.concat({
                        name : accountName,
                        startDate : new Date().toLocaleDateString(),
                        endDate : '',
                        members : 0
                    })
                }
            })
        }
        
        this.setState({ modalAddVisible : false})
    }

    editAccount(newName){
        if(this.validateAccountName(newName) === undefined){
            let oldName = this.state.currentAccountName
            let accounts = this.state.accounts.map((account) => {
                if (account.name === oldName) return { ...account, name : newName }
                return account
            })
            this.setState({accounts : accounts})
        }
        this.setState({ modalEditVisible : false})
    }

    finishAccount() {
        // Primero finalizar en bd... luego:
        this.removeAccount(this.state.currentAccountName)

        this.setState({ modalFinishVisible : false})
    }

    deleteAccount() {
        // Primero eliminar en bd... luego:
        this.removeAccount(this.state.currentAccountName)

        this.setState({ modalDeleteVisible : false})
    }

    validateAccountName(accountName){
        return this.state.accounts.find((account) => {
            if (normalizeString(account.name) === normalizeString(accountName)) return true
        })
    }

    removeAccount(name){
        let index = this.state.accounts.findIndex((account) => account.name === name)
        if(index !== undefined && index >= 0){
            this.setState(state => {
                state.accounts.splice(index, 1)
                return {
                    accounts : state.accounts
                }
            })
        }
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    containerList: {
        padding: 20,
        paddingTop: 0,
    },
    initText: {
        color: Color.TEXT_LIGHT,
        fontSize: 17,
        padding: 50,
        textAlign: 'center'
    },
    arrow: {
        position: 'absolute',
        bottom: 75,
        right: 75,
    },
    modalContainer:{
        backgroundColor: Color.WHITE,
        padding: 20,
        borderRadius: 5,
    },
    modalTitle: {
        color: Color.TEXT_LABEL,
        fontSize: 20,
        marginBottom: 10,
        fontWeight: 'bold',
    },
    modalButton: {
        padding: 20,
        borderRadius: 7,
        marginTop: 15,
        alignItems: 'center',
    },
    modalButtonText: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    inputFilter : {
        backgroundColor: Color.TEXT_LABEL,
        color: Color.WHITE,
        textAlign: 'center'
    }
    
})