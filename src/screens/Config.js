import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'

import { Color } from './../constants/Color'

export default class ConfigScreen extends Component {
    static navigationOptions = {
        title: 'Configuración',
        headerStyle: {
            backgroundColor: Color.PRIMARY,
        },
        headerTintColor: Color.WHITE,
    }

    constructor(props){
        super(props)
    }

    render(){
        return(
            <View>
                <Text>Configuración</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
})