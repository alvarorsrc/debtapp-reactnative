import React, { Component } from 'react'
import { ScrollView, TextInput, StyleSheet } from 'react-native'

import { Color } from '../constants/Color'

export default class DetailScreen extends Component {
    static navigationOptions = {
        title: 'Detalles',
        headerStyle: {
            backgroundColor: Color.PRIMARY,
        },
        headerTintColor: Color.WHITE,
    }

    constructor(props){
        super(props)
        this.state = {
            member : {
                name : '',
                startDate : '',
                detail : []
            },
            products : [
                { name : 'lomo', price : 6},
                { name : 'chaufa', price : 6},
                { name : 'saltado', price : 7},
                { name : 'inka personal', price : 2},
            ],
            currentProduct: 'lomo'
        }
    }

    render(){
        return(
            <ScrollView contentContainerStyle = {styles.containerList}>

                <TextInput
                    value = {this.state.member.name}
                    style = {styles.inputFilter}
                    placeholder = "Ingrese el nombre del miembro"
                    autoFocus = {true}
                    onChangeText = {(name) => this.setState( state => {
                        return {
                            member : {...state.member, name}
                        }
                    })}
                />

            </ScrollView>
        )
    }

}


const styles = StyleSheet.create({
    containerList: {
        padding: 20,
        paddingTop: 0,
    },
})