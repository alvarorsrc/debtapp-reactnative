import React, { Component } from 'react'
import { Text,Image, StatusBar,View, StyleSheet } from 'react-native'
import {NavigationActions, StackActions} from 'react-navigation'
import { Color } from './../constants/Color'
import SvgCustom from './../components/SvgCustom'

export default class LoadingScreen extends Component {
    static navigationOptions = {
        title: 'Home',
        header: null,
    };

    constructor(props){
        super(props)
        //Simulación de consumo de api o carga de datos
        setTimeout(()=>{
            const resetAction = StackActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName: 'Main' }),
                ]
            })
            this.props.navigation.dispatch(resetAction)
        }, 2000)
    }
    render() {
        return (
            <View style = {styles.container}>
                <StatusBar backgroundColor={Color.PRIMARY} hidden={true} />
                <SvgCustom
                    name   = "debtLogo"
                    width  = {80}
                    height = {80}
                    color  = {Color.GRAY_LIGHT_2}
                />
                <Text style = {styles.text}>Debt App</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        backgroundColor: Color.PRIMARY,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text : {
        color: Color.GRAY_LIGHT,
        fontFamily: 'Chewy-Regular',
        fontSize : 35,
        marginTop : 10,
    }
})