import React, { Component } from 'react';
import {View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import { Color } from './../constants/Color'

export default class RegisterScreen extends Component {
    static navigationOptions = {
        title: 'Registrar',
        headerStyle: {
            backgroundColor: Color.PRIMARY,
        },
        headerTintColor: Color.WHITE,
    }

    render() {
        return (
            <View style = {styles.container}>
                <View>
                    <Text style = {styles.label}>
                        Email
                    </Text>
                    <TextInput
                        style = {styles.input}
                        placeholder = "Escriba su correo electrónico"
                    />
                    <Text style = {styles.label}>
                        Nombre
                    </Text>
                    <TextInput
                        style = {styles.input}
                        placeholder = "Escriba su nombre"
                    />
                    <Text style = {styles.label}>
                        Contraseña
                    </Text>
                    <TextInput
                        style = {styles.input}
                        placeholder = "Escriba su contraseña"
                    />
                    <Text style = {styles.label}>
                        Repita su Contraseña
                    </Text>
                    <TextInput
                        style = {styles.input}
                        placeholder = "Escriba nuevamente su contraseña"
                    />
                </View>
                <TouchableOpacity 
                    style = {styles.button}
                    onPress={()=>{
                        
                    }}
                >
                    <Text style = {{color: Color.WHITE,fontSize: 16,}}>
                        Registrar
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }    
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    label: {
        fontSize: 18,
        color: Color.TEXT_LABEL,
        fontWeight: 'bold',
    },
    input: {
        fontSize: 16,
        borderBottomWidth: 1,
        borderBottomColor: Color.LINE,
        marginBottom: 15,
    },
    button: {
        marginTop: 15,
        padding: 15,
        paddingLeft: 25,
        paddingRight: 25,
        borderRadius: 3,
        backgroundColor: Color.SECOND,
    },
})