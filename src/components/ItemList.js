import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'

import { Color } from './../constants/Color'
import SvgCustom from './SvgCustom'

export default class ItemList extends Component {

    constructor(props){
        super(props)
        this.state = {
            sideOptions : false
        }
    }

    render(){
        return(
            <TouchableOpacity style={ styles.container }
                onPress = {() => this.props.onPress()}
                onLongPress = {() => this.setState((state)=>{ return {sideOptions : !state.sideOptions} })}>
                <View>
                    <Text style = {styles.mainText}>
                        {this.props.mainText}
                    </Text>
                    <Text style = {styles.subText}>
                        {this.props.subText}
                    </Text>
                </View>
                <View>
                    {this.state.sideOptions
                        ?   <View style = { styles.sideContainer }>
                                {!this.props.onlyDelete &&
                                    <TouchableOpacity style = { styles.sideOption }
                                        onPress = {()=> {
                                            this.setState({sideOptions : false})
                                            this.props.onEdit()
                                        }}>
                                        <SvgCustom name = "edit"
                                            width  = {22}
                                            height = {22}
                                            color  = {Color.BLUE_PRIMARY}/>
                                    </TouchableOpacity>
                                }
                                {!this.props.onlyDelete &&
                                    <TouchableOpacity style = { styles.sideOption }
                                        onPress = {()=>{
                                            this.setState({sideOptions : false})
                                            this.props.onFinish()
                                        }}>
                                        <SvgCustom name = "finish"
                                            width  = {22}
                                            height = {22}
                                            color  = {Color.BLUE_PRIMARY}/>
                                    </TouchableOpacity>
                                }
                                <TouchableOpacity style = { styles.sideOption }
                                    onPress = {()=>{
                                        this.setState({sideOptions : false})
                                        this.props.onDelete()
                                    }}>
                                    <SvgCustom name = "delete"
                                        width  = {22}
                                        height = {22}
                                        color  = {Color.RED_SECOND}/>
                                </TouchableOpacity>
                            </View>
                        :
                            <Text style = {styles.sideText}>
                                {this.props.sideText}
                            </Text>
                    }
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({    
    container:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: .5,
        borderBottomColor: Color.PURPLE_LIGHT,
        padding: 18,
    },
    mainText:{
        color: Color.SECOND,
        fontSize: 17,
    },
    subText:{
        color: Color.TEXT_LIGHT,
    },
    sideText:{
        color: Color.SECOND,
        fontSize: 16,
    },
    sideContainer:{
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    sideOption:{
        backgroundColor: Color.TEXT_LABEL,
        padding: 10,
        borderRadius: 50,
        marginRight: 5,
    },
})