import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet } from 'react-native'

import { Color } from './../constants/Color'
import SvgCustom from './SvgCustom'

export default class FabButton extends Component {

    constructor(props){
        super(props)
    }

    render(){
        return(
            <TouchableOpacity
                style   = {styles.floatButton}
                onPress = {() => this.props.onTap()}
            >
                <SvgCustom
                    name   = "plus"
                    width  = {20}
                    height = {20}
                    color  = {Color.WHITE}
                />
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({    
    floatButton: {
        backgroundColor: Color.BLUE_PRIMARY,
        padding: 20,
        borderRadius: 50,
        position: 'absolute',
        bottom: 15,
        right: 15,
    },
})