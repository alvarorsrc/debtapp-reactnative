import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity, StyleSheet } from 'react-native'

import { Color } from './../constants/Color'

export default class InputDialog extends Component {

    constructor(props){
        super(props)
        let defaultFirst = !!this.props.defaultFirst ? this.props.defaultFirst : ''
        this.state = {
            name : defaultFirst
        }

        this.defaultFirst = defaultFirst

    }

    render(){
        return(
            <View style={ styles.container}>
                <Text style={styles.title}>
                    {this.props.title}
                </Text>
                <TextInput
                    value = {this.state.name}
                    style = {styles.input}
                    placeholder = {this.props.phFirst}
                    autoFocus = {true}
                    onChangeText = {(name) => this.setState({name : name})}/>
                <View style={styles.buttonsContainer}>
                    <TouchableOpacity 
                        style={styles.button}
                        onPress = {() => this.props.onCancel()}>
                        <Text style={styles.buttonText}>
                            {!!this.props.cancelText ? this.props.cancelText.toUpperCase() : 'CANCELAR'}
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.button}
                        onPress = {() => this.props.onConfirm(this.state.name, this.defaultFirst)}>
                        <Text
                            style={[styles.buttonText, {fontWeight: 'bold', color: this.props.color}]}>
                            {!!this.props.confirmText ? this.props.confirmText.toUpperCase() : 'ACEPTAR'}
                        </Text>
                    </TouchableOpacity>                    
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({    
    container:{
        backgroundColor: Color.WHITE,
        padding: 20,
        paddingBottom: 0,
        borderRadius: 5,
    },
    title: {
        color: Color.TEXT_LABEL,
        fontSize: 20,
        marginBottom: 10,
        fontWeight: 'bold',
    },
    input:{
        fontSize: 16,
        marginTop: 10,
        borderWidth: .5,
        borderColor: Color.TEXT_LABEL,
        color: Color.TEXT_LABEL,
    },
    buttonsContainer:{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: 15,
    },
    button: {
        padding: 15,
        paddingTop: 25,
        paddingBottom: 25,
        alignItems: 'center',
    },
    buttonText: {
        fontSize: 16,
    },
})